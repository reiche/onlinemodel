import numpy as np

class Dechirper:
    def __init__(self):

        self.Z0 = 377
        self.c  = 299792458
        self.eps=8.854e-12
        self.mks=1./4/np.pi/self.eps

        self.N=10000
        # OK for python2 and python3
        self.lwake=list(range(self.N))
        self.twake=list(range(self.N))
        self.qwake=list(range(self.N))
        self.s=list(range(self.N))


    
    def getWake(self,gap, element='', writeSDDS=''):

        # emperical length of single particle wake
        print('Calculating Dechirper Wake for gap: %e m' % gap)
        s0 = 0.5e-3     
        if 'SINSB05' in element:
            s0 = 10e-3
        self.s= np.linspace(0,s0,num=self.N)
        # parameter for SwissFEL dechirper
        a     = gap/2
        p     = 0.500e-3
        delta = 0.250e-3  # h in paper SwisSFEL 250 micron
        g     = 0.250e-3  # t in paper
        alfa = 1-0.465*np.sqrt(g/p)-0.07*g/p
        s0r  = a*a*g/2/np.pi/alfa/alfa/p/p
        s0l  = s0r*9./4.
        s0q  = s0r*(15./16)**2
        s0d  = s0r*(15./14)**2

        sl=self.s/s0l
        sq=self.s/s0q
        sd=self.s/s0d

        # longitudinal wake
        self.lwake=np.pi*np.pi/a/a/4*np.exp(-np.sqrt(sl))*self.mks
        # quadrupole wake
        self.qwake=(np.pi/2/a)**4*s0q*(1-(1+np.sqrt(sq))*np.exp(-np.sqrt(sq)))*self.mks
        # dipole wake
        self.twake=(np.pi/2/a)**4*s0d*(1-(1+np.sqrt(sd))*np.exp(-np.sqrt(sd)))*self.mks


        if len(writeSDDS)>0:
            self.writeSDDS(element, writeSDDS)
 

        return [self.s, self.lwake, self.twake, self.qwake]    

#--------------------------------------
#   write SDDS wakefile


    def writeSDDS(self,element, path):
        filename=path+'/wake_%s.sdds' % element
        fid=open(filename,'w')
        fid.write('SDDS1\n')
        fid.write('&column name=z,    units=m,    type=double,    &end\n')
        fid.write('&column name=W,    units=V/C,  type=double,    &end\n')
        fid.write('&column name=t,    units=s,    type=double,    &end\n')
        fid.write('&column name=WD,   units=V/C/m,    type=double,    &end\n')
        fid.write('&column name=WQ,   units=V/C/m,    type=double,    &end\n')
        fid.write('&column name=WQP,  units=V/C/m,    type=double,    &end\n')
        fid.write('&data mode=ascii, &end\n')
        fid.write('! page number 1\n')
        fid.write('%i\n' % len(self.s))
        for i in range(len(self.s)):
            fid.write('  %12.6e  %12.6e  %12.6e  %12.6e  %12.6e  %12.6e \n' % (self.s[i],self.lwake[i],self.s[i]/self.c,self.twake[i],self.qwake[i],-self.qwake[i]))
        fid.close()



