from time import gmtime, strftime
from OMAppTemplate import ApplicationTemplate
from OMSwissFELmagnet import SwissFELmagnet
from OMSwissFELrf import SwissFELrf
import PyCafe

#--------
# Epics Channel Interface

class EpicsChannel(ApplicationTemplate):
    def __init__(self,prefix='',path='.'):
        ApplicationTemplate.__init__(self)
        self.switch=0
        self.path=path
        self.BPM=[]
        self.Screen=[]
        self.Bend=[]
        self.Bendp=[] # polarities of the bend magnets
        self.Corrector=[]
        self.Quad=[]
        self.PQuad=[]
        self.Sext=[]
        self.Undulator=[]
        self.PhaseShifter=[]
        self.RF=[]
        self.Solenoid=[]
        self.Collimator=[]
        self.Slit=[]
        self.ICT=[]
        self.Foil=[]
        self.SRmonitor=[]
        self.CurrMonitor=[]
        self.Wire=[]
        self.BAM=[]
        self.BS=[]  # Beam stopper
        self.ALL={}
        self.QuadMotor=[]
        self.RFsysRegistered=[]
        self.RFsystem=[]
        self.Cavities={}
        self.BPMatr=['Q1','X1','Y1','SIM-X1','SIM-Y1','OFFS-X','OFFS-Y','Q1-VALID','X1-VALID','Y1-VALID','Q2','X2','Y2','SIM-X2','SIM-Y2','Q2-VALID','X2-VALID','Y2-VALID']
        self.ScreenAtr=['GET-SCREEN']
        self.ScreenAtr2=['ROI-XMAX','ROI-XMIN','ROI-YMAX','ROI-YMIN','SIG-X-GAUSS','SIG-Y-GAUSS']
        self.QuadAtr=['KL','I-SET','PS-MODE']
        self.PQuadAtr=['KL']
        self.CorrAtr=['KICK','I-SET','PS-MODE']
        self.BendAtr=['ANGLE','I-SET','PS-MODE']
        self.SolenoidAtr=['I-SET','PS-MODE']
        self.SextAtr=['K2L','I-SET','PS-MODE']
        #self.RFatr=['GRADIENT','PHASE','PHASE-LLRF','PHASE-ONCREST','PHASE-BC1','PHASE-BC2','PHASE-LH']
        self.RFatr=[]
        self.BSatr=['IN-OUT']
        self.UndulatorMotorAtr=['X-SET','Y-SET','X-READ','Y-READ','XMIN','XMAX','YMIN','YMAX','NPOINT','NACQ','NINTERVX','NINTERVY','PITCH-SET','YAW-SET','PITCH-READ','YAW-READ','GAP-SET','GAP-READ']
        self.PQuadMotorAtr=['X','IN-OUT','NACQ','NINTERVX','NINTERVY']
        self.QuadMotorAtr=['X','Y','XMIN','XMAX','YMIN','YMAX']
        # Handle created for a minimal set of attributes 
        self.RFsysAtr=['SET-ACC-VOLT',
                       'SET-BEAM-PHASE',
                       'SET-KLY-POWER',
                       'SET-VSUM-PHASE',
                       #'SET-DELTA-PHASE',
                       #'CMD-DELTA-PHASE',
                       #'SET-STATION-MODE',
                       'SET-STATION-STATE',
                       'SET-VSUM-AMPLT-SCALE',
                       #'SET-PHASE-OFFSET',
                       'SET-VSUM-PHASE-OFFSET-BASE',
                       'SET-VSUM-PHASE-OFFSET-CORRLH',
                       'SET-VSUM-PHASE-OFFSET-CORRBC1',
                       'SET-VSUM-PHASE-OFFSET-CORRBC2',
                       'SET-VSUM-PHASE-OFFSET-CORRENA',
                       'SET-VOLT-POWER-SCALE',
                       #'SET-PULSE-DAC-DELAY',
                       'GET-ACC-VOLT',
                       #'GET-ACC-VOLT-ERR',
                       #'GET-ACC-VOLT-STAB',
                       #'GET-ACC-VOLT-STAB-REACHED',
                       'GET-BEAM-PHASE',
                       #'GET-BEAM-PHASE-REF',
                       'GET-KLY-POWER',
                       #'GET-KLY-POWER-ERR',
                       'GET-VSUM-PHASE',
                       #'GET-VSUM-PHASE-REF',
                       #'GET-PHASE-ERR',
                       #'GET-PHASE-STAB',
                       #'GET-PHASE-STAB-REACHED',
                       #'GET-AMPLT-REACHED',
                       #'GET-PHASE-REACHED',
                       #'GET-RF-REACHED',
                       'GET-STATION-MODE',
                       'GET-STATION-STATE',
                       'GET-VSUM-AMPLT',
                       'GET-RF-READY-STATUS',
                       'GET-BEAM-CALIB-STATUS',
                       'GET-VSUM-PHASE-OFFSET']

        self.fid=None
        if len(prefix):
            self.prefix=prefix+'-'
        else:
            self.prefix=prefix
        self.ch={} # Epics channels (together with relevant info) in memory
        self.Handles={}
        self.cafe=PyCafe.CyCafe()
        self.cafe.init()
        self.cyca=PyCafe.CyCa()
        # soft channels for optics at Injector(SB02 exit) and elsewhere
        self.OpticsLocation=['SINSB02.EXIT','SINLH03.EXIT']
        self.OpticsAtr=['BETX','ALFX','BETY','ALFY','EXN','EYN','CHARGE']
        self.Chicane=['SINLH02.LH','SINBC02.BC','S10BC02.BC'] # This could/should come from SF.BC
        self.ChicaneAtr=['ANGLE-SET','MOTOR-POS-SET','DL','DL-VALID']
        # Importing magnet data from OMSwissFELMagnet...
        SM=SwissFELmagnet()
        self.QuadSpec=SM.QuadSpec
        self.BendSpec=SM.BendSpec
        self.CorrSpec=SM.CorrSpec 
        self.reviveChannel=None
        self.ScreenOpened=[]
        del(SM)
        # Importing rf spec from OMSwissFELrf...
        RF=SwissFELrf()
        self.RFspec=RF.RFspec
        del(RF)
        
        self.N2Z=0 # None to zero in self.get1

    def openStream(self,name='SwissFEL'):
        path=self.path
        filename='%s/%s.subs' % (path,name)
        self.fid=open(filename,'w')
        
    def closeStream(self):
        self.fid.close()
            
    def write(self,line):
        self.fid.write(line)

    def demandMapID(self):
        'OMEpicsChannel always requests Map ID to Layout manger'
        return 1

#----------------
#  the element specific format


    def isType(self,name):
        return 0

    def switchOutput(self,switch=1):
        self.switch=switch
        if switch=='Memory' or switch=='memory':
            self.switch=1
        elif switch=='File' or switch=='file':
            self.switch=0

    def getHolyList(self):
        import xlrd
        import urllib.request
        import json
        #with open('typelist.json') as jf:    
        #    tl = json.load(jf)
        tl=["DALA", "DBAM", "DBCM", "DBLM", "DBPM", "DCOL", "DDRM", "DEOM",
               "DHVS", "DICT", "DLLM", "DLLP", "DSCR", "DSFH", "DSFV", "DSRM",
               "DTAU", "DWCM", "DWSC", "MBND", "MCOR", "MKAC", "MKDC", "MQUA",
               "MSEX", "MSOL", "RACC", "RGUN", "RKLY", "RTDS", "RWVG", "SDMP",
               "SGST", "SLOS", "SSTP", "UIND", "UPHS"]
        url = "http://gfa-reports.psi.ch/GFA_Report.aspx?dbname=GFAPRD&table=HL_Alias+HL_Revision+SwissFEL_HL_XLS_V+SwissFEL_HL_XLS_PHASE1_V"
        xf = urllib.request.urlretrieve(url)
        book = xlrd.open_workbook(xf[0])
        sh = book.sheet_by_index(0)               # sheet 0 = phase1
        HolyList = {}
        for t in tl:
            device = []
            for rx in range(sh.nrows):
                if sh.cell_value(rowx=rx, colx=2) == t:
                    device.append((sh.cell_value(rowx=rx, colx=3)))
            HolyList[t] = device
        urllib.request.urlcleanup()
        return HolyList

    def writeLine(self,line,seq):
        now=strftime("%Y-%m-%d %H:%M:%S", gmtime())

        if self.MapIndx>=0:
            return


        # At the end of writeFacility, let's write-out

        

        if len(line.Name)==0 and self.switch==0: 
            sp=''.ljust(len(self.prefix))
            
            self.fid.write('# softIOC for Virtual SwissFEL in office network\n')
            self.fid.write('#--------------------------------------------------------------------\n')
            self.fid.write('#\n')
            self.fid.write('# $Author: automatically generated with Online Model MOdule OMEpicsChannel.py$\n') 
            self.fid.write('# $Date: '+now+' $\n')
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('#--------------------------------------------------------------------\n')
            self.fid.write('\n')

            # BPM
            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('# simple interface records for Virtual SwissFEL in office network\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_DBPM.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.BPM:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')
            
            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_DBPM_reference.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE              '+sp+'AXIS}\n')
            for ele in self.BPM:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+' X }\n')
                self.fid.write('        {'+EpicsName+' Y }\n')
            self.fid.write('}\n')

            # Screen
            self.fid.write('file S_VA_DSCR.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.Screen:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            # Collimator
            self.fid.write('file S_VA_DCOL.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        { DEVICE               '+sp+'}\n')

            for ele in self.Collimator:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # Slit
            self.fid.write('file S_VA_DHVS.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE            '+sp+'}\n')

            for ele in self.Slit:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # ICT
            self.fid.write('file S_VA_DICT.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE            '+sp+'}\n')

            for ele in self.ICT:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # Foil
            self.fid.write('file S_VA_DSFV.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.Foil:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # Synchrotron radiation monitor
            self.fid.write('file S_VA_DSRM.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.SRmonitor:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            # Wall current monitor
            self.fid.write('file S_VA_DWCM.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                 '+sp+'}\n')

            for ele in self.CurrMonitor:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # Wire scanner
            self.fid.write('file S_VA_DWSC.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.Wire:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            self.fid.write('file S_VA_DBAM.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.BAM:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            try:
                HolyList=self.getHolyList()
                self.fid.write('file S_VA_DBLM.template { \n')
                self.fid.write('        pattern\n')
                self.fid.write('        {DEVICE               '+sp+'}\n')
                for ele in HolyList['DBLM']:
                    EpicsName=self.prefix+ele.ljust(21)
                    self.fid.write('        {'+EpicsName+'}\n')
                self.fid.write('}\n')

                self.fid.write('file S_VA_DDRM.template { \n')
                self.fid.write('        pattern\n')
                self.fid.write('        {DEVICE               '+sp+'}\n')
                for ele in HolyList['DDRM']:
                    EpicsName=self.prefix+ele.ljust(21)
                    self.fid.write('        {'+EpicsName+'}\n')
                self.fid.write('}\n')

                self.fid.write('file S_VA_DLLM.template { \n')
                self.fid.write('        pattern\n')
                self.fid.write('        {DEVICE               '+sp+'}\n')
                for ele in HolyList['DLLM']:
                    EpicsName=self.prefix+ele.ljust(21)
                    self.fid.write('        {'+EpicsName+'}\n')
                self.fid.write('}\n')

            except:
                print('Failed to read HolyList')

            # Solenoid
            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_MSOL.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            Registered=[]
            for ele in self.Solenoid:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                # To avoid duplication of dipole at branching, which could not be done in writeDipole 
                if EpicsName not in Registered:
                    self.fid.write('        {'+EpicsName+'}\n')
                    Registered.append(EpicsName)
            self.fid.write('}\n')

            # Dipole
            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_MBND.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            Registered=[]
            for ele in self.Bend:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                # To avoid duplication of dipole at branching, which could not be done in writeDipole 
                if EpicsName not in Registered:
                    self.fid.write('        {'+EpicsName+'}\n')
                    Registered.append(EpicsName)
            self.fid.write('}\n')



            # Corrector
            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_MCOR.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')



            for ele in self.Corrector:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                if 'MQUA' in EpicsName:
                    self.fid.write('        {'+EpicsName.replace('MQUA','MCRX')+'}\n')
                    self.fid.write('        {'+EpicsName.replace('MQUA','MCRY')+'}\n')
                elif 'MBND' in EpicsName:
                    self.fid.write('        {'+EpicsName.replace('MBND','MCRX')+'}\n')
                elif 'UIND' in EpicsName:
                    self.fid.write('        {'+EpicsName.replace('UIND','MCRX')+'}\n')
								#elif ele.__dict__.has_key('corx') and ele.__dict__.has_key('cory'):		
                elif 'corx' in ele.__dict__ and 'cory' in ele.__dict__:
                    self.fid.write('        {'+EpicsName.replace('MCOR','MCRX')+'}\n')
                    self.fid.write('        {'+EpicsName.replace('MCOR','MCRY')+'}\n')
								#elif ele.__dict__.has_key('corx'):		
                elif 'corx' in ele.__dict__:
                    self.fid.write('        {'+EpicsName.replace('MCOR','MCRX')+'}\n')
								#elif ele.__dict__.has_key('cory'):									
                elif 'cory' in ele.__dict__:
                    self.fid.write('        {'+EpicsName.replace('MCOR','MCRY')+'}\n')
                else:
                    # this block is just for debugging.
                    print('What about this corrector?:  ',ele.Name)
                
            self.fid.write('}\n')

            # Quad

            QImax=[]
            Qsub={}
            for ele in self.Quad:
                Type=ele.Baugruppe
                Imax=str(int(self.QuadSpec[Type][2])) 
                if Imax not in QImax:
                    QImax.append(Imax)
                    Qsub[Imax]=[]
                Qsub[Imax].append(ele)

            for k in Qsub.keys():
                

                self.fid.write('#--------------------------------------------------------------------\n') 
                self.fid.write('#\n')
                self.fid.write('#\n')
                self.fid.write('\n')
                self.fid.write('file S_VA_MQUA.template { \n')
                self.fid.write('        pattern\n')
                self.fid.write('        {DEVICE               '+sp+'IMAX}\n')

                for ele in Qsub[k]:
                    EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                    self.fid.write('        {'+EpicsName+'     '+str(k)+'}\n')
                self.fid.write('}\n')

            # QUarupole motor (Undulator section)
            
            self.fid.write('file S_VA_MQUAMotor.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            for ele in self.QuadMotor:
                EpicsName=self.prefix+ele.Name.replace('.','-')+'-MOT'
                EpicsName=EpicsName.ljust(21+len(self.prefix))
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            # Permanent magnet quad (Alignment quad)
                

            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_MQUAP.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')
            
            for ele in self.PQuad:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # Permanent magnet quad (Motor)
            
            self.fid.write('file S_VA_MQUAPMotor.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            for ele in self.PQuad:
                EpicsName=self.prefix+ele.Name.replace('.','-')+'-MOT'
                EpicsName=EpicsName.ljust(21+len(self.prefix))
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            
            # Sext
            self.fid.write('#--------------------------------------------------------------------\n') 
            self.fid.write('#\n')
            self.fid.write('#\n')
            self.fid.write('\n')
            self.fid.write('file S_VA_MSEX.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE               '+sp+'}\n')

            for ele in self.Sext:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            
            # RF
            self.fid.write('file S_VA_RF.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            for ele in self.RF:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            
            # RFsystem
            self.fid.write('file S_VA_RFsystem.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                       DRVH}\n')


            for ele in self.RFsystem:
                EpicsName=self.prefix+ele.replace('.','-').ljust(21)
                if 'EG' in EpicsName:
                    DRVH=self.RFspec['PSI-GUN'][1]
                elif 'CB14' in EpicsName:
                    DRVH=self.RFspec['C-BAND-TDS'][1]
                elif 'SINDI' in EpicsName:
                    DRVH=self.RFspec['S-BAND-TDS'][1]
                elif 'XB' in EpicsName:
                    DRVH=self.RFspec['X-BAND-ACC'][1]
                elif 'SB' in EpicsName:
                    DRVH=self.RFspec['S-BAND-ACC'][1]
                elif 'CB' in EpicsName:
                    DRVH=self.RFspec['C-BAND-ACC'][1]
                else: # This should not be the case
                    DRVH=0
                    
                self.fid.write('        {'+EpicsName+'    '+str(DRVH)+'}\n')
            self.fid.write('}\n')

            # Undulator
            self.fid.write('file S_VA_UIND.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            for ele in self.Undulator:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')
            


            # Undulator(Motor)
            self.fid.write('file S_VA_UINDMotor.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            for ele in self.Undulator:
                EpicsName=self.prefix+ele.Name.replace('.','-')+'-MOT'
                EpicsName=EpicsName.ljust(21+len(self.prefix))
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            
            # Optics channels
            self.fid.write('file S_VA_Optics.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            
            for ele in self.OpticsLocation:
                EpicsName=self.prefix+ele.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

            # BeamStopper
            self.fid.write('file S_VA_SSTP.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')


            for ele in self.BS:
                EpicsName=self.prefix+ele.Name.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')
            


            # Chicanes
            self.fid.write('file S_VA_Chicane.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')
            
            
            for ele in self.Chicane:
                EpicsName=self.prefix+ele.replace('.','-').ljust(21)
                self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')
            

            # Laser
            self.fid.write('file S_VA_Laser.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')
            
            EpicsName=self.prefix+'SINSS-LPSA'.ljust(21)
            self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')


            # Dummy
            self.fid.write('file S_VA_Dummy.template { \n')
            self.fid.write('        pattern\n')
            self.fid.write('        {DEVICE                   '+sp+'}\n')
            
            EpicsName=self.prefix+'DUMMY'.ljust(21)
            self.fid.write('        {'+EpicsName+'}\n')
            self.fid.write('}\n')

        elif len(line.Name)==0 and self.switch==1: # Epics channel list in memory

            
            # This part needs to be improved when we implement more and more channels...

            # Energy Channel for all the elements
            self.ch['Energy']=[] 

            # BPM
            for atr in self.BPMatr:
                key='BPM:'+atr
                self.ch[key]=[]
                for ele in self.BPM:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)

            
            for ele in self.BPM:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')
                

            # Screen
            for atr in self.ScreenAtr:
                key='Screen:'+atr
                self.ch[key]=[]
                for ele in self.Screen:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)

            
            for ele in self.Screen:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')

            # Solenoid
            for atr in self.SolenoidAtr:
                key='Solenoid:'+atr
                self.ch[key]=[]
                for ele in self.Solenoid:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
                
            for ele in self.Solenoid:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')

            # Dipole
            for atr in self.BendAtr:
                key='Bend:'+atr
                self.ch[key]=[]
                for ele in self.Bend:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
                
            for ele in self.Bend:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')
                if 'design_angle' in dir(ele): 
                    if ele.design_angle>0:
                        self.Bendp.append(1)
                    else:
                        self.Bendp.append(-1)
                else:
                    self.Bendp.append(1)

            # Quad

            for atr in self.QuadAtr:
                key='Quad:'+atr
                self.ch[key]=[]
                for ele in self.Quad:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
            
            for ele in self.Quad:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')

            # Quad magnet motor (Motorized quadrupole in the undulator section)
            qm_name=[]
            for atr in self.QuadMotorAtr:
                key='QuadMotor:'+atr
                self.ch[key]=[]
                for ele in self.QuadMotor:
                    EpicsName=self.prefix+ele.Name.replace('.','-')+'-MOT'
                    self.ch[key].append(EpicsName+':'+atr)
                    if (ele.Name+'.MOT') not in qm_name:
                        qm_name.append(ele.Name+'.MOT')

            # Permanent magnet quad (Alignement quad)
            for atr in self.PQuadAtr:
                key='PQuad:'+atr
                self.ch[key]=[]
                for ele in self.PQuad:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
            
            
            for ele in self.PQuad:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')
                

            # Permanent magnet quad (Alignement quad)
            for atr in self.PQuadMotorAtr:
                key='PQuadMotor:'+atr
                self.ch[key]=[]
                pqm_name=[]
                for ele in self.PQuad:
                    EpicsName=self.prefix+ele.Name.replace('.','-')+'-MOT'
                    self.ch[key].append(EpicsName+':'+atr)
                    if (ele.Name+'.MOT') not in pqm_name:
                        pqm_name.append(ele.Name+'.MOT')

            # Sext


            for atr in self.SextAtr:
                key='Sext:'+atr
                self.ch[key]=[]
                for ele in self.Sext:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
            
            for ele in self.Sext:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')



            # Corrector
            for atr in self.CorrAtr:
                key='Corrector:'+atr
                self.ch[key]=[]
                Registered=[]
                for ele in self.Corrector:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    if EpicsName not in Registered:
                        Registered.append(EpicsName)
                        #if ele.__dict__.has_key('cor') or ele.__dict__.has_key('corx'):
                        if 'cor' in ele.__dict__ or 'corx' in ele.__dict__:
                            name=EpicsName.replace('MQUA','MCRX').replace('MBND','MCRX').replace('UIND','MCRX').replace('MCOR','MCRX')
                            self.ch[key].append(name+':'+atr)
												#if ele.__dict__.has_key('cory'):		
                        if 'cory' in ele.__dict__:
                            name=EpicsName.replace('MQUA','MCRY').replace('MBND','MCRY').replace('UIND','MCRY').replace('MCOR','MCRY')
                            self.ch[key].append(name+':'+atr)
                        #self.ch[key].append(EpicsName+':'+atr)

                        '''
                        if 'MQUA' in EpicsName:
                            EpicsName1=EpicsName.replace('MQUA','MCRX')
                            self.ch[key].append(EpicsName1+':'+atr)
                            EpicsName=EpicsName.replace('MQUA','MCRY')
                        elif 'MBND' in EpicsName:
                            EpicsName=EpicsName.replace('MBND','MCRX')
                        elif 'UIND' in EpicsName:
                            EpicsName=EpicsName.replace('UIND','MCRX')
                        elif ele.__dict__.has_key('corx') and ele.__dict__.has_key('cory'):
                            EpicsName1=EpicsName.replace('MCOR','MCRX')
                            self.ch[key].append(EpicsName1+':'+atr)
                            EpicsName=EpicsName.replace('MCOR','MCRY')
                        elif ele.__dict__.has_key('corx'):
                            EpicsName=EpicsName.replace('MCOR','MCRX')
                        elif ele.__dict__.has_key('cory'):
                            EpicsName=EpicsName.replace('MCOR','MCRY')
                        self.ch[key].append(EpicsName+':'+atr)
                        '''

            CName=[]
            Registered=[]
            for ele in self.Corrector:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                if EpicsName not in Registered:
                    Registered.append(EpicsName)
                    if 'MQUA' in EpicsName:
                        EpicsName1=EpicsName.replace('MQUA','MCRX')
                        self.ch['Energy'].append(EpicsName1+':ENERGY')
                        CName.append(ele.Name.replace('MQUA','MCRX'))
                        EpicsName=EpicsName.replace('MQUA','MCRY')
                        CName.append(ele.Name.replace('MQUA','MCRY'))
                    elif 'MBND' in EpicsName:
                        EpicsName=EpicsName.replace('MBND','MCRX')
                        CName.append(ele.Name.replace('MBND','MCRX'))
                    elif 'UIND' in EpicsName:
                        EpicsName=EpicsName.replace('UIND','MCRX')
                        CName.append(ele.Name.replace('UIND','MCRX'))
										#elif ele.__dict__.has_key('corx') and ele.__dict__.has_key('cory'):		
                    elif 'corx' in ele.__dict__ and 'cory' in ele.__dict__:
                        EpicsName1=EpicsName.replace('MCOR','MCRX')
                        self.ch['Energy'].append(EpicsName1+':ENERGY')
                        CName.append(ele.Name.replace('MCOR','MCRX'))
                        EpicsName=EpicsName.replace('MCOR','MCRY')
                        CName.append(ele.Name.replace('MCOR','MCRY'))
										#elif ele.__dict__.has_key('corx'):		
                    elif 'corx' in ele.__dict__:
                        EpicsName=EpicsName.replace('MCOR','MCRX')
                        CName.append(ele.Name.replace('MCOR','MCRX'))
										#elif ele.__dict__.has_key('cory'):		
                    elif 'cory' in ele.__dict__:
                        EpicsName=EpicsName.replace('MCOR','MCRY')
                        CName.append(ele.Name.replace('MCOR','MCRY'))
                    self.ch['Energy'].append(EpicsName+':ENERGY')



            # RF
            for atr in self.RFatr:
                key='RF:'+atr
                self.ch[key]=[]
                for ele in self.RF:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
            
            for ele in self.RF:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')

            # RFsystem
            for atr in self.RFsysAtr:
                key='RFsystem:'+atr
                self.ch[key]=[]
                for en in self.RFsystem:
                    EpicsName=self.prefix+en.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)

            # Undulator(Motor)
            for atr in self.UndulatorMotorAtr:
                key='UndulatorMotor:'+atr
                self.ch[key]=[]
                for ele in self.Undulator:
                    EpicsName=self.prefix+ele.Name.replace('.','-')+'-MOT'
                    self.ch[key].append(EpicsName+':'+atr)
            
            um_name=[]
            for ele in self.Undulator:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')
                um_name.append(ele.Name+'.MOT')
                

            # Beam stopper
            for atr in self.BSatr:
                key='BeamStopper:'+atr
                self.ch[key]=[]
                for ele in self.BS:
                    EpicsName=self.prefix+ele.Name.replace('.','-')
                    self.ch[key].append(EpicsName+':'+atr)
            
            for ele in self.BS:
                EpicsName=self.prefix+ele.Name.replace('.','-')
                self.ch['Energy'].append(EpicsName+':ENERGY')



            def ele2Name(elems,ALL):
                Name=[]
                for ele in elems:
                    Name.append(ele.Name)
                    ALL.append(ele.Name)

                return Name

            ALL=[]
            self.ALL['BPM']=ele2Name(self.BPM,ALL)
            self.ALL['Screen']=ele2Name(self.Screen,ALL)
            #ele2Name(self.Screen,ALL)
            self.ALL['Solenoid']=ele2Name(self.Solenoid,ALL)
            self.ALL['Bend']=ele2Name(self.Bend,ALL)
            self.ALL['Quad']=ele2Name(self.Quad,ALL)
            self.ALL['PQuad']=ele2Name(self.PQuad,ALL)
            self.ALL['Sext']=ele2Name(self.Sext,ALL)
            #self.ALL['Corrector']=ele2Name(self.Corrector,ALL) # for correctors, it is not that simple...
            self.ALL['Corrector']=CName
            ALL=ALL+CName
            self.ALL['RF']=ele2Name(self.RF,ALL)
            self.ALL['BeamStopper']=ele2Name(self.BS,ALL)
            self.ALL['RFsystem']=self.RFsystem
            self.ALL['Undulator']=ele2Name(self.Undulator,ALL)
            self.ALL['Energy']=ALL
            
            self.ALL['UndulatorMotor']=um_name # These device do not have the energy attribute, ':ENERGY'.
            self.ALL['QuadMotor']=qm_name
            self.ALL['PQuadMotor']=qm_name

            # Optics, different structure exceptionaly
            for key in self.OpticsLocation:
                self.ch[key]=[]
                EpicsName=self.prefix+key.replace('.','-')
                for atr in self.OpticsAtr:
                    self.ch[key].append(EpicsName+':'+atr)

                self.ALL[key]=self.ch[key]


            # Screen, different structure exceptionaly
            for s in self.Screen:
                key=s.Name
                self.ch[key]=[]
                EpicsName=self.prefix+key.replace('.','-')
                for atr in self.ScreenAtr2:
                    self.ch[key].append(EpicsName+':'+atr)

                self.ALL[key]=self.ch[key]


                
            # BC and LH, different structure exceptionaly
            for c in self.Chicane:
                self.ch[c]=[]
                for atr in self.ChicaneAtr:
                    EpicsName=self.prefix+c.replace('.','-')
                    self.ch[c].append(EpicsName+':'+atr)

                self.ALL[c]=self.ch[c]

            # Laser
            self.ALL['Laser']=[self.prefix+'SINSS-LPSA:SHUTTER']
            self.ch['Laser']=[self.prefix+'SINSS-LPSA:SHUTTER']


    def writeDrift(self,ele):
        return

    def writeVacuum(self,ele):
        if 'BEAM-STOP' in ele.Baugruppe:
            self.BS.append(ele)
        return

    def writeAlignment(self,ele):
        return

    def writeBend(self,ele):
        # This if-block cannot exclude the duplicated dipole for the beam dump
        # because they are different instance
        if ele not in self.Bend:
            self.Bend.append(ele)
						#if ele.__dict__.has_key('cor'):
            if 'cor' in ele.__dict__:
                self.Corrector.append(ele)
        return

    def writeQuadrupole(self,ele):     
        if (ele not in self.Quad) and (ele not in self.PQuad):
            if ele.Baugruppe=='QFU':
                self.PQuad.append(ele)
            else:
                self.Quad.append(ele)
								#if ele.__dict__.has_key('corx') or ele.__dict__.has_key('cory'):
                if 'corx' in ele.__dict__ or 'cory' in ele.__dict__:
                    self.Corrector.append(ele)
        if (ele not in self.PQuad) and ('SARUN' in ele.Name): # MOtorized quadrupoles
                self.QuadMotor.append(ele)
            
        return
   
    def writeCorrector(self,ele):   
		    #if ele.__dict__.has_key('corx') or ele.__dict__.has_key('cory'):         
        if 'corx' in ele.__dict__ or 'cory' in ele.__dict__:
            self.Corrector.append(ele)
        return

    def writeSextupole(self,ele):       
        self.Sext.append(ele)
        return
       
    def writeRF(self,ele=None):
        self.RF.append(ele)
        sec=ele.Name.split('.')[0]
        if sec not in self.RFsysRegistered:
            self.RFsysRegistered.append(sec)
            dn=ele.Name.replace('RACC','RSYS').replace('RGUN','RSYS').replace('RTDS','RSYS')
            self.RFsystem.append(dn)
            self.Cavities[dn]=[ele.Baugruppe,ele.Length,ele.Name]
        else:
            self.Cavities[self.RFsystem[-1]].append(ele.Name)
        return

    def writeUndulator(self,ele): 
        if 'UIND' in ele.Name:
            self.Undulator.append(ele)
        elif 'UPHS' in ele.Name: # to be implemented...
            self.PhaseShifter.append(ele)
				#if ele.__dict__.has_key('cor'): # Geomagnetism compensation coil		
        if 'cor' in ele.__dict__: # Geomagnetism compensation coil
            self.Corrector.append(ele)
        return

    def writeDiagnostic(self,ele):
        if 'DBPM' in ele.Name:
            self.BPM.append(ele)
        if 'DSCR' in ele.Name:
            self.Screen.append(ele)
        if 'DCOL' in ele.Name:
            self.Collimator.append(ele)
        if 'DHVS' in ele.Name:
            self.Slit.append(ele)
        if 'DICT' in ele.Name:
            self.ICT.append(ele)
        if 'DSFV' in ele.Name:
            self.Foil.append(ele)
        if 'DSRM' in ele.Name:
            self.SRmonitor.append(ele)
        if 'DWCM' in ele.Name:
            self.CurrMonitor.append(ele)
        if 'DWSC' in ele.Name:
            self.Wire.append(ele)
        if 'DBAM' in ele.Name:
            self.BAM.append(ele)


        return

    

    def writeMarker(self,ele):
        return


    def writeSolenoid(self,ele):
        self.Solenoid.append(ele)
        return


    def createHandle(self,inlist=None,exlist=[]):
        if self.Handles:
            # Handles are created already
            return
        self.cafe.openGroupPrepare()
        for key in self.ch.keys():
            if key not in exlist:
                if inlist:
                    if key in inlist:
                        #FullList.append(self.ch[key])
                        self.Handles[key]=self.cafe.grouping(key, self.ch[key])
                        if not self.reviveChannel:
                            self.reviveChannel=self.ch[key][0]
                else:
                    #FullList.append(self.ch[key])
                    self.Handles[key]=self.cafe.grouping(key, self.ch[key])
                    if not self.reviveChannel:
                        self.reviveChannel=self.ch[key][0]

        if inlist:
            if len(inlist)<30:
                wait=len(inlist)
            else:
                wait=30
        else:
            wait=30
        self.cafe.openGroupNowAndWait(wait)


    def addGroup(self,GroupName,ChList):
        self.cafe.openGroupPrepare()
        self.Handles[GroupName]=self.cafe.grouping(GroupName, ChList)
        if not self.ch:
            self.reviveChannel=ChList[0]
        self.ch[GroupName]=ChList
        self.cafe.openGroupNowAndWait(0.1)
        
        dlist=[]
        for ch in ChList:
            dlist.append(ch.split(':')[0])
        self.ALL[GroupName]=dlist


    def createHandleOld(self,inlist=None,exlist=[]):
        if self.Handles:
            # Handles are created already
            return
        FullList=[]
        for key in self.ch.keys():
            if key not in exlist:
                if inlist:
                    if key in inlist:
                        FullList.append(self.ch[key])
                else:
                    FullList.append(self.ch[key])

        self.reviveChannel=FullList[0][0]
        #self.cafe.grouping(self.ch[key])
        
        self.cafe.openGroupPrepare()
            
        #fullhandle=self.cafe.groupings(FullList)
        v=[]
        for i in range(0,len(FullList)):
            
            chlist=FullList[i]
            vi=self.cafe.grouping(chlist[0], chlist)
            v.append(vi)
        fullhandle=v
        if inlist:
            if len(inlist)<30:
                wait=len(inlist)
            else:
                wait=30
        else:
            wait=30
        self.cafe.openGroupNowAndWait(wait)

        i=0
        for key in self.ch.keys():
            if key not in exlist:
                if inlist:
                    if key in inlist:
                        self.Handles[key]=fullhandle[i]
                        i=i+1
                else:
                    self.Handles[key]=fullhandle[i]
                    i=i+1
        Ntotal=0
        for l in FullList:
            Ntotal=Ntotal+len(l)


    def getHandle(self,key):
        if key not in self.Handles.keys():
            print('OMEpicsChannel: No such a handle',key)
            return None,None

        dkey=key.split(':')[0]

        handle=self.Handles[key]
        DN=self.ALL[dkey]  # Device name as in LayoutManager
        return handle,DN

    def getDeviceList(self,key):
        #keylist=['BPM','RF','Bend','Corrector','Quad','Sext','Energy']
        if key not in self.ALL.keys():
            return None,None

        DN=self.ALL[key]  # Device name as in LayoutManager
        return DN

    def put(self,key,values,StatBack=0):


        handle,DN=self.getHandle(key)
        if handle==None:
            return


        if not isinstance(values,list):
            return


        s,slist=self.cafe.setGroup(handle,values)

        if StatBack:
            return s,slist

    def get(self,key,StatBack=0,dtype='native',dictout=0):


        handle,DN=self.getHandle(key)
        if handle==None:
            return

        [values, s, slist]=self.cafe.getGroup(handle,dtype)

        if dictout:
            dic=dict(zip(DN,values))
            if StatBack:
                return DN,dic,s,slist
            else:
                return DN,dic
        else:
            if StatBack:
                return DN,values,s,slist
            else:
                return DN,values

    def put1(self,name,val):
        s=self.cafe.set(name,val)
        return s

    def get1(self,name,dtype='native'):

        v=self.cafe.get(name,dt=dtype)
        if self.N2Z and v==None:
            v=0
        return v


    def putList(self,name,value):
        if not isinstance(name,list):
            return
        if len(name)>len(value):
            return
        for i in range(0,len(name)):
            vi=self.cafe.set(name[i],value[i])

    def getList(self,name):
        if not isinstance(name,list):
            return None
        v=[]
        for n in name:
            vi=self.cafe.get(name)
            v.append(vi)
        return v



    def putScreenImage(self,name,Image):
        # Method for Virtual Accelerator
        EpicsName=self.prefix+name.replace('.','-')+':FPICTURE'
        if EpicsName not in self.ScreenOpened:
            self.ScreenOpened.append(EpicsName)
            h=self.cafe.open(EpicsName)
        self.cafe.set(EpicsName,Image)

    def getScreenImage(self,name):
        # Used in Virtual accelerator but general function
        EpicsName=self.prefix+name.replace('.','-')+':FPICTURE'
        if EpicsName not in self.ScreenOpened:
            self.ScreenOpened.append(EpicsName)
            self.cafe.open(EpicsName)
        Image=self.cafe.getArray(EpicsName)


        return Image

    def revive(self,ch=None):
        # This is for threading. See OMDaemon.py for example.
        #self.cafe.reviveGroup(self.reviveChannel)
        if ch:
            reviveChannel=ch
        else:
            reviveChannel=self.reviveChannel
        self.cafe.attachContext(reviveChannel)

    def generateChannelList(self,include=[]):

        ch=[]
        for cl in self.ch.values():
            for c in cl:
                filt=1
                for inc in include:
                    if inc not in c:
                        filt=0
                if filt:
                    ch.append(c)

        return ch


    def switchException(self,switch):
        if switch==1:
            self.cafe.withExceptions(True)
            self.N2Z=0
        elif switch==0:
            self.cafe.withExceptions(False)
            self.N2Z=0
        elif switch==-1:
            self.cafe.withExceptions(False)
            self.N2Z=1
