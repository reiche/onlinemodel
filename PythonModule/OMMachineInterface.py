from OMMagnetCalibration import *
from OMUndulatorCalibration import *
from OMEpicsChannel import *

class MachineInterface:
    
    def __init__(self,SF=None,VA=False):
        self.VA=VA
        self.filter={'Section':[],'Type':[],'Element':[]}
        self.initialized=False

        if not SF:
            print('Facility instance is mandatory')
            return

        # check for VA accelerator or real machine
        if VA:
            self.EC=EpicsChannel('VA')
        else:    
            self.EC=EpicsChannel()

        # initialize the epics connection via CAFE    
        self.EC.switchOutput('memory')   
        SF.writeFacility(self.EC,1)    
        # select the channels to be updated
        self.EC.createHandle(['Quad:I-SET','Bend:I-SET','Corrector:I-SET','RFsystem:SET-ACC-VOLT','RFsystem:SET-BEAM-PHASE','UndulatorMotor:X-SET','RFsystem:GET-RF-READY-STATUS','RFsystem:GET-STATION-MODE','RFsystem:GET-STATION-STATE','RFsystem:SET-VSUM-PHASE'])

        # initialize calibrations of magnets, RF and undulator
        self.calMagnet   =SwissFELMagnet()
        SF.writeFacility(self.calMagnet)
        self.calUndulator=SwissFELUndulator()
        SF.writeFacility(self.calUndulator)
#        self.calRF       =SwissFELRF()

        self.initialized=True;


    def updateFilter(self,field,values):
        if field in self.filter.keys():
            if isinstance(values,list):
                self.filter[field]=values
            else:
                print('Filter values must be a list')
        else:
            print('Only Section, Type and Element are allowed for filter types')

    def applyFilter(self, dn, val, typ):
        res1=[]
        res2=[]
        if (len(self.filter['Type'])==0) or (typ in self.filter['Type']):
            for i in range(len(dn)):
                sec=dn[i].split('.')[0]
                if (len(self.filter['Section'])==0) or (sec in self.filter['Section']):
                    if (len(self.filter['Element'])==0) or (dn[i] in self.filter['Element']):
                        res1.append(dn[i])
                        res2.append(val[i])                
        return res1,res2        

#-------------------
# functions to set the machine from values in the online model            
    def updateMachine(self,SF=None):
        if not SF:
            return
        print('Not implemented yet....')
       

    def writeRF2MAchine(self,SF=None):

        if not SF or not self.initialized:
            return
        # First, get list of RFs
        #dn=EC.getDeviceList('RF')
        RFsystem=EC.getDeviceList('RFsystem')

        KPhase=[0]*len(RFsystem)
        KVoltage=[0]*len(RFsystem)
        for k in EC.Cavities.keys():
            c=EC.Cavities[k]
            ele=SF.getElement(c[2])
            KPhase[RFsystem.index(k)]=ele.Phase
            KVoltage[RFsystem.index(k)]=ele.Gradient*ele.Length*(len(c)-2)


        EC.put('RFsystem:SET-ACC-VOLT',KVoltage)
        EC.put('RFsystem:SET-BEAM-PHASE',KPhase)
        return

 #----------------
# function to update the model with machine settings
       
    def updateModel(self,SF=None):
        self.updateRF(SF)
        self.updateQuadrupoles(SF)
        self.updateBends(SF)
        self.updateCorrectors(SF)
        




    def updateRF(self,SF=None,inCallBack=0,GunNominal=1):
        if not SF or not self.initialized:
            return



        #---------------------------------------------------------------
        # taken from OMEnergyMAnager.py:UpdateFromEpics
        
        # note that the old PhaseOffset has been taken out because it is defined that all RF station have a beam phase.
        # whether it is callibrated or not it is out of scope of this module


        #dn,Phase=EC.get('RF:PHASE') # Assuming that the PHASE-ONCREST is mesaured and set
        #dn,Gradient=EC.get('RF:GRADIENT')

        dn=self.EC.getDeviceList('RF')  # get a list of all RF stations

        if inCallBack==1: # Note that "get" does not work in callbacks...
            klystron=self.EC.getDeviceList('RFsystem')

            Status,s,sl =self.EC.cafe.getGroupCache('RFsystem:GET-RF-READY-STATUS',dt='int')
            Mode,s,sl   =self.EC.cafe.getGroupCache('RFsystem:GET-STATION-MODE')
            State,s,sl  =self.EC.cafe.getGroupCache('RFsystem:GET-STATION-STATE')
            V,s,sl      =self.EC.cafe.getGroupCache('RFsystem:SET-ACC-VOLT')
            KPhase,s,sl =self.EC.cafe.getGroupCache('RFsystem:SET-VSUM-PHASE')
            KPhaseB,s,sl=self.EC.cafe.getGroupCache('RFsystem:SET-BEAM-PHASE')

        elif inCallBack==2: # Monitor is assigned to individual channel...
            klystron=self.EC.getDeviceList('RFsystem')
            Status=[]
            Mode=[]
            State=[]
            V=[]
            KPhase=[]
            KPhaseB=[]
            for r in klystron: 
                c=self.EC.cafe.getPVCache(self.EC.prefix+r.replace('.','-')+':GET-RF-READY-STATUS',dt='int')
                Status.append(c.value[0])
                c=self.EC.cafe.getPVCache(self.EC.prefix+r.replace('.','-')+':GET-STATION-MODE')
                Mode.append(c.value[0])
                c=self.EC.cafe.getPVCache(self.EC.prefix+r.replace('.','-')+':GET-STATION-STATE')
                State.append(c.value[0])
                c=self.EC.cafe.getPVCache(self.EC.prefix+r.replace('.','-')+':SET-ACC-VOLT')
                V.append(c.value[0])
                c=self.EC.cafe.getPVCache(self.EC.prefix+r.replace('.','-')+':SET-VSUM-PHASE')
                KPhase.append(c.value[0])
                c=self.EC.cafe.getPVCache(self.EC.prefix+r.replace('.','-')+':SET-BEAM-PHASE')
                KPhaseB.append(c.value[0])
        else:
# normal mode to pull directly the values from the epics channels
            klystron,Status =self.EC.get('RFsystem:GET-RF-READY-STATUS',0,'int')
            klystron,Mode   =self.EC.get('RFsystem:GET-STATION-MODE')
            klystron,State  =self.EC.get('RFsystem:GET-STATION-STATE')
            klystron,V      =self.EC.get('RFsystem:SET-ACC-VOLT')
            klystron,KPhase =self.EC.get('RFsystem:SET-VSUM-PHASE')
            klystron,KPhaseB=self.EC.get('RFsystem:SET-BEAM-PHASE')
        # Check if the rf stations are in operation
        kok={}
        for i in range(0,len(klystron)):
            sec=klystron[i].split('.')[0]
            ele=SF.getRegExpElement(sec,'R....00','Length')  # starting with R and an index of multiple of 100
            Totlen=ele[0]*len(ele)
            if State[i]=='RF ON PHAFB' or State[i]=='RF ON AMPFB' or State[i]=='RF ON FB':
                State[i]='RF ON'
            if Mode[i]=='NORMAL' and State[i]=='RF ON' and Status[i]:
                kok[sec]=[V[i]/Totlen,KPhase[i],KPhaseB[i],0]
            else:               
                kok[sec]=[0,KPhase[i],KPhaseB[i],0]



        # RF:PHASE and RF:GRADIENT should be constructed from RFsystem:**-PHASE and RFsystem:**-VOLT/POWER in Virtual accelerator
        # This is not yet done. Taking into account calibration constants is not straightforward, and SET-ACC-VOLT is used for both real and virtual machine at this moment. 2016.06.24

        Phase=[0]*len(dn)
        PhaseB=[0]*len(dn)
        PhaseOffsetRF=[0]*len(dn)
        Gradient=[0]*len(dn)
        for i in range(0,len(dn)):
            sec=dn[i].split('.')[0]
            Gradient[i]=kok[sec][0]
            Phase[i]=kok[sec][1]
            PhaseB[i]=kok[sec][2]
            PhaseOffsetRF[i]=kok[sec][3]

        print(dn,PhaseB,Gradient)    
        SF.setGroup(dn,'Phase',PhaseB)
        SF.setGroup(dn,'Gradient',Gradient)
        if GunNominal:
            Gun=SF.getElement('SINEG01.RGUN100')
            Gun.Gradient=33e6
            Gun.Phase=90
        SF.writeFacility(SF.EM) # Re-compute the energy at all the elements

        return    


    def updateQuadrupoles(self,SF=None):    
        if not SF or not self.initialized:
            return
        #get from machine the current setvalues of quadrupole currents. Use the baugruppen type to get right callibration file
        dn,I=self.EC.get('Quad:I-SET')
        dn,I=self.applyFilter(dn,I,'Quadrupole')
        QuadKL=self.calMagnet.QuadrupoleI2KL(dn,I,SF.EM)
        QuadK =self.calMagnet.QuadrupoleKL2K(dn,QuadKL)
        SF.setGroup(dn,'k1',QuadK)
        
  
    def updateBends(self,SF=None):
        if not SF or not self.initialized:
            return

        dn,I=self.EC.get('Bend:I-SET')
        dn,I=self.applyFilter(dn,I,'Bend')
        Angle=self.calMagnet.BendI2Angle(dn,I,SF.EM)
        for idx in range(0,len(dn)):
            print('Setvalues for Dipoles: ',dn[idx],I[idx], Angle[idx])
        SF.setGroup(dn,'angle',Angle)

            
    def updateCorrectors(self,SF=None):
        if not SF or not self.initialized:
            return

        dn,I=self.EC.get('Corrector:I-SET')
        dn,I=self.applyFilter(dn,I,'Corrector')
        CorrKick=self.calMagnet.CorrectorI2Kick(dn,I,SF.EM)
        for i in range(0,len(dn)):
            name=dn[i]
            sec=name[0:7]
            device='M...'+name[12:15]
            if 'CRX' in name:
                SF.setRegExpElement(sec,device,'corx',CorrKick[i])
            else: 
                if 'CRY' in dn[i]:
                    SF.setRegExpElement(sec,device,'cory',CorrKick[i])
                else:
                    #these are only the resonant and dc kick of septum
                    SF.setGroup([dn[i]],'cory',[CorrKick[i]])

