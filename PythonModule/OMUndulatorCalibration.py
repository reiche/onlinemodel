import math
import sys
import json
import numpy as np
from OMAppTemplate import ApplicationTemplate

class SwissFELUndulator(ApplicationTemplate):
    def __init__(self):
        ApplicationTemplate.__init__(self)
        self.data={}   # dictionary to hold all calibration data
        self.file='/afs/psi.ch/intranet/SF/Applications/config/UndulatorConfig.json'  # path to dictionary
        self.readJson()
        self.UndulatorType={} # dictionary of magnet names with baugruppe

#----------------------------
# input and output of Json file        

    def readJson(self):
# read configuration json file. If it does not exist, it will be generated.
        
        try:            
            datafile=open(self.file)
        except IOError:
            print('Error: Cannot find configuration file: %s' % self.file)
            print(' --> Generating template file')
            self.generateJson()
            self.writeJson()
            return

        self.data=json.load(datafile)
        datafile.close()
        return

    def writeJson(self):
        with open(self.file,'w') as outfile:
            json.dump(self.data,outfile,sort_keys=True,indent=4,ensure_ascii=False)
        return
        
    def generateJson(self):
        self.data={}
        self.data['U15_41802']={'Type':'U15','lambda':0.015,'K0':4.43246,'a':4.60076,'b':1.21304}
        self.data['U15_41694']={'Type':'U15','lambda':0.015,'K0':4.48098,'a':4.15106,'b':0.37087}
        self.data['U15']={'Ref':'U15_41802'}
        self.data['U50']={'Type':'U50','lambda':0.050,'K0':9.6426,'a':3.312,'b':0} 

        return

#--------------------------------------------------
# setting up the references to the magnet names if they are not fully defined.

 
    def writeUndulator(self,ele):
        if ele.Name not in self.UndulatorType.keys():
            self.UndulatorType[ele.Name]=ele.Baugruppe
        return 


#----------------------------------------------------------------------------------
# utility functions to simplify the access functions


    def getSpec(self,ele):

        if isinstance(ele,str):
            Name=ele
            if Name in self.data.keys():
                spec=self.data[Name]
                if 'Ref' in spec.keys():
                    spec=self.getSpec(str(spec['Ref']))
                return spec  
            else:
                if Name in self.UndulatorType.keys():
                    return self.getSpec(self.UndulatorType[Name])
                else:
                    print('Calibration has no entry for %s' % Name)
                    return False
        else:
            return self.getSpec(Name,typ)



#----------------------------------------------------------
# access function

    def UndulatorGap2K(self,ele,g):
        spec=self.getSpec(ele)
        lam=spec['lambda'];
        K0=spec['K0']
        a=spec['a']
        b=spec['b']
        x=g/lam;
        return K0*math.exp(-a*x+b*x*x)

    def UndulatorK2Gap(self,ele,K):
        spec=self.getSpec(ele)
        lam=spec['lambda'];
        K0=spec['K0']
        a=spec['a']
        b=spec['b']
        y=math.log(K/K0)
        if (b>0):
            y=y/b
            a=a/b
            x=0.5*a-math.sqrt(0.25*a*a+y)
        else:
            x=-y/a
        return x*lam



