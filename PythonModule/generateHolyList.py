

from OMFacility import *
from OMMachineInterface import *

#-------------------------------
# initiate the sequence: Names, energy estimate etc.


SwissFEL =Facility(1,0)
MI=MachineInterface(SwissFEL,True)  # work with VA
MI.updateFilter('Element',['SARUN17.MQUA080'])
MI.updateModel(SwissFEL)

