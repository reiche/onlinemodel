import sys
#sys.path.insert(0, '/afs/psi.ch/intranet/SF/Applications/Development/OnlineModel/lib')
#sys.path.insert(0, '/afs/psi.ch/intranet/SF/Applications/Development/Masa/git/lib')

#import readline
#import PyCafe

import threading as thr

from time import sleep
import math

class SwissFELrf():
    def __init__(self):
        self.RFspec={}
        # V_cavity=a*sqrt(P_klystron)
        # For now, self.RFspec[cavity]=[a], and 'a' is arbitrary determined.
        # Other specifications can be added if needed
        # Specification cavity by cavity later?
        # Format (Very temporary) [a, Max SET-ACC-VOLT /station], where 'a' is the coefficient to connect the power and voltage
        self.RFspec['X-BAND-ACC']=[24.0,40]
        self.RFspec['C-BAND-ACC']=[12.0,240]
        self.RFspec['S-BAND-ACC']=[6.0,140]
        self.RFspec['PSI-GUN']=[3.0,8]
        self.RFspec['S-BAND-TDS']=[4.0,40]
        self.RFspec['C-BAND-TDS']=[8.0,100]
        



    def connectChannels(self,EC):
        # Method for Virtual accelerator

        def cb_power(h):       
            Cav=EC.Cavities[handle[h][0]]
            Ncav=len(Cav)-2 # Note that Cav[0] and Cav[1] are used for cavity type and length
            CavType=Cav[0]
            a=self.RFspec[CavType][0]

            if handle[h][1]==':SET-KLY-POWER':
                en=EC.prefix+handle[h][0].replace('.','-')+handle[h][1]
                c=EC.cafe.getPVCache(en)
                P=c.value[0]
                en=EC.prefix+handle[h][0].replace('.','-')+':GET-KLY-POWER'
                EC.put1(en,P) #propagate the set value to the read value
                
                en1=EC.prefix+handle[h][0].replace('.','-')+':SET-ACC-VOLT'
                en2=EC.prefix+handle[h][0].replace('.','-')+':GET-ACC-VOLT'
                V=a*math.sqrt(P)
                EC.put1(en1,V) # Propagete the power set to the on-crest voltage (sum of all cavities) 
                EC.put1(en2,V)

                #en=EC.prefix+handle[h][0].replace('.','-')+':SET-ON-CREST-ENERGY-GAIN'
                #EC.put1(en,V) # Propagete the same voltage to the energy gain channel



            elif handle[h][1]==':SET-ACC-VOLT':
                en=EC.prefix+handle[h][0].replace('.','-')+handle[h][1]
                c=EC.cafe.getPVCache(en)
                V=c.value[0]
                
                en=EC.prefix+handle[h][0].replace('.','-')+':GET-ACC-VOLT'
                EC.put1(en,V) # Propagete the set value to the read value

                P=(V/a)**2
                en1=EC.prefix+handle[h][0].replace('.','-')+':SET-KLY-POWER'
                en2=EC.prefix+handle[h][0].replace('.','-')+':GET-KLY-POWER'
                EC.put1(en1,P) #propagate the set value to the Klystron power
                EC.put1(en2,P)

            else:
                print('????!!!!',handle)

        def cb_phase(h):

            en=EC.prefix+handle[h][0].replace('.','-')+':SET-PHASE-OFFSET'
            c=EC.cafe.getPVCache(en)
            phase_offset=c.value[0]



            Cav=EC.Cavities[handle[h][0]]
            Ncav=len(Cav)-2 # Note that Cav[0] and Cav[1] are used for cavity type and length

            if handle[h][1]==':SET-BEAM-PHASE':
                
                en=EC.prefix+handle[h][0].replace('.','-')+handle[h][1]
                c=EC.cafe.getPVCache(en)
                b_phase=c.value[0]
                en=EC.prefix+handle[h][0].replace('.','-')+':GET-BEAM-PHASE'
                EC.put1(en,b_phase) #propagate the set value to the read value
                
                en1=EC.prefix+handle[h][0].replace('.','-')+':SET-VSUM-PHASE'
                phase=b_phase+phase_offset-90

                EC.put1(en1,phase) # Propagete the phase change to VSUM-PHASE

            
            elif handle[h][1]==':SET-VSUM-PHASE':
               
                en=EC.prefix+handle[h][0].replace('.','-')+handle[h][1]
                c=EC.cafe.getPVCache(en)
                phase=c.value[0]
                en=EC.prefix+handle[h][0].replace('.','-')+':GET-VSUM-PHASE'
                EC.put1(en,phase) #propagate the set value to the read value
                
                en1=EC.prefix+handle[h][0].replace('.','-')+':SET-BEAM-PHASE'
                b_phase=phase-phase_offset+90
                EC.put1(en1,b_phase) # Propagete the phase change to BEAM-PHASE



            elif handle[h][1]==':SET-ON-CREST-VSUM-PHASE' or handle[h][1]==':ON-CREST-PHASE-VALID' \
                 or handle[h][1]==':PHASE-DELAY' or handle[h][1]==':PHASE-DELAY-VALID':
                # Change BEAM-PHASE-OFFSET
                en=EC.prefix+handle[h][0].replace('.','-')+':SET-ON-CREST-VSUM-PHASE'
                c=EC.cafe.getPVCache(en)
                on_phase=c.value[0]

                en=EC.prefix+handle[h][0].replace('.','-')+':ON-CREST-PHASE-VALID'
                c=EC.cafe.getPVCache(en)
                valid1=c.value[0]

                en=EC.prefix+handle[h][0].replace('.','-')+':PHASE-DELAY'
                c=EC.cafe.getPVCache(en)
                d_phase=c.value[0]

                en=EC.prefix+handle[h][0].replace('.','-')+':PHASE-DELAY-VALID'
                c=EC.cafe.getPVCache(en)
                valid2=c.value[0]

                phase_offset=0
                if valid1:
                    phase_offset=phase_offset+on_phase
                if valid2:
                    phase_offset=phase_offset+d_phase
                
                en1=EC.prefix+handle[h][0].replace('.','-')+':SET-PHASE-OFFSET'
                EC.put1(en1,phase_offset)

            elif handle[h][1]==':SET-PHASE-OFFSET':

                en=EC.prefix+handle[h][0].replace('.','-')+':SET-BEAM-PHASE'
                c=EC.cafe.getPVCache(en)
                b_phase=c.value[0]
                phase=b_phase+phase_offset-90

                en1=EC.prefix+handle[h][0].replace('.','-')+':SET-VSUM-PHASE'
                EC.put1(en1,phase) # Don't change BEAM-PHASE but change VSUM-PHASE such that BEAM-PHASE is kept constant

            else:
                print('Other case exists!!!!',handle[h],h)
                EC.cafe.printHandle(h)
                          
                

        def cb_switch(h):    

            en=EC.prefix+handle[h][0].replace('.','-')+handle[h][1]
            c=EC.cafe.getPVCache(en)
            val=c.value[0]
            en=EC.prefix+handle[h][0].replace('.','-')+handle[h][1].replace('SET','GET')
            EC.put1(en,val) 
                

        # Power
        # There are four possible input channeles:
        # 1) RFsystem:SET-KLY-POWER, for experts and operators don't touch
        # 2) RFsystem:SET-ACC-VOLT, use this channel normally
        # 3) RFsystem:SET-ON-CREST-ENERGY-GAIN, for experts and operators don't touch
        # 4) RFsystem:SET-ON-CREST-VSUM-AMPLT, for experts and operators don't touch


        # Phase
        # Input channel is always RFsystem:SET-BEAM-PHASE or RFsystem:SET-VSUM-PHASE
        # The beam phase RFsystem:SET-BEAM-PHASE is determined as 'VSUM-PHASE'-'ON-CREST-VSUM-PHASE'+90,
        # where ON-CREST-VSUM-PHASE needs to be found from beam measurement: scan the beam energy as a function of VSUM-PHASE
        # and 'VSUM-PHASE' giving the maximum energy should be set to ON-CREST-VSUM-PHASE.
        # When SET-BEAM-PHASE is varied, 'VSUM-PHASE'='SET-BEAM-PHASE'+'ON-CREST-VSUM-PHASE'-90 accordingly
        # Now, the phase delay due to non-straight bunch compressor is taken into account.
        # When BC is turned on, the beam arrives later. To compensate the delay, 'VSUM-PHASE'='SET-BEAM-PHASE'+'ON-CREST-VSUM-PHASE'-90-'PHASE-DELAY'
        # such the effective 'BEAM-PHASE' is kept constant.

        # Switching on the rf systems when called
        attr=[':SET-STATION-STATE',
              ':GET-STATION-STATE',
              ':SET-STATION-MODE',
              ':GET-STATION-MODE',
              ':GET-RF-READY-STATUS']

        val=[1]*len(EC.RFsystem)
        val2=[2]*len(EC.RFsystem)
        
        
        for ar in attr:
            key='RFsystem'+ar
            if 'MODE' in ar:
                EC.put(key,val2)
            else:
                EC.put(key,val)

        #dn=EC.getDeviceList('RF')

        handle={}
        htemp={}
        RFthr=[]
        #for k in EC.RFsystem:
        moID={}
        EC.cafe.openPrepare()
        for i in range(0,len(EC.RFsystem)):
            k=EC.RFsystem[i]
            c=EC.Cavities[k]
            #g=Gradient[dn.index(c[2])]
            #V=g*c[1]*(len(c)-2)
            #a=self.RFspec[c[0]][0]
            #P=(V/a)**2
            
            h1=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-KLY-POWER')
            h2=EC.cafe.open(EC.prefix+k.replace('.','-')+':GET-KLY-POWER')
            h3=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-ACC-VOLT')
            h4=EC.cafe.open(EC.prefix+k.replace('.','-')+':GET-ACC-VOLT')
            h5=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-BEAM-PHASE')
            h6=EC.cafe.open(EC.prefix+k.replace('.','-')+':GET-BEAM-PHASE')
            h7=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-VSUM-PHASE')
            h8=EC.cafe.open(EC.prefix+k.replace('.','-')+':GET-VSUM-PHASE')
            h9=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-ON-CREST-VSUM-PHASE')
            h10=EC.cafe.open(EC.prefix+k.replace('.','-')+':ON-CREST-PHASE-VALID')
            h11=EC.cafe.open(EC.prefix+k.replace('.','-')+':PHASE-DELAY')
            h12=EC.cafe.open(EC.prefix+k.replace('.','-')+':PHASE-DELAY-VALID')
            h13=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-PHASE-OFFSET')
            h14=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-STATION-STATE')
            h15=EC.cafe.open(EC.prefix+k.replace('.','-')+':SET-STATION-MODE')

            ht=EC.cafe.open(EC.prefix+k.replace('.','-')+':GET-STATION-STATE')
            ht=EC.cafe.open(EC.prefix+k.replace('.','-')+':GET-STATION-MODE')
            htemp[k]=[h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]
        EC.cafe.openNowAndWait(15)

        

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]
            #m0=EC.cafe.monitorStart(h1, cb=cb_power, dbr=EC.cyca.CY_DBR_TIME) # value, status, severity, time
            #m0=EC.cafe.monitorStart(h1, cb=cb_power, dbr=EC.cyca.CY_DBR_STS) # value, status, severity
            
            handle[h1]=[k,':SET-KLY-POWER']
            m0=EC.cafe.monitorStart(h1, cb=cb_power, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE) # CY_DBR_PLAIN only returns value
            moID[h1]=m0
  
        EC.cafe.openMonitorNowAndWait(2)

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]

            handle[h3]=[k,':SET-ACC-VOLT']
            m0=EC.cafe.monitorStart(h3, cb=cb_power, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h3]=m0

        EC.cafe.openMonitorNowAndWait(2)

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]


            handle[h5]=[k,':SET-BEAM-PHASE']
            m0=EC.cafe.monitorStart(h5, cb=cb_phase, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h5]=m0

        EC.cafe.openMonitorNowAndWait(2)

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]

            handle[h7]=[k,':SET-VSUM-PHASE']
            m0=EC.cafe.monitorStart(h7, cb=cb_phase, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h7]=m0

        EC.cafe.openMonitorNowAndWait(2)

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]


            handle[h9]=[k,':SET-ON-CREST-VSUM-PHASE']
            m0=EC.cafe.monitorStart(h9, cb=cb_phase, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h9]=m0

        EC.cafe.openMonitorNowAndWait(2)

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]


            handle[h10]=[k,':ON-CREST-PHASE-VALID']
            m0=EC.cafe.monitorStart(h10, cb=cb_phase, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h10]=m0

        EC.cafe.openMonitorNowAndWait(2)


        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]


            handle[h11]=[k,':PHASE-DELAY']
            m0=EC.cafe.monitorStart(h11, cb=cb_phase, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h11]=m0


        EC.cafe.openMonitorNowAndWait(2)


        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]

            handle[h12]=[k,':PHASE-DELAY-VALID']
            m0=EC.cafe.monitorStart(h12, cb=cb_phase, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h12]=m0

        EC.cafe.openMonitorNowAndWait(2)



        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]

            handle[h14]=[k,':SET-STATION-STATE']
            m0=EC.cafe.monitorStart(h14, cb=cb_switch, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h14]=m0

        EC.cafe.openMonitorNowAndWait(2)

        EC.cafe.openMonitorPrepare()
        for k in EC.RFsystem:
            [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15]=htemp[k]

            handle[h15]=[k,':SET-STATION-MODE']
            m0=EC.cafe.monitorStart(h15, cb=cb_switch, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            moID[h15]=m0

        EC.cafe.openMonitorNowAndWait(2)


        sleep(0.1)

        print('connecting done')
            
